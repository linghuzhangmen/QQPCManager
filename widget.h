﻿#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include <QMouseEvent>

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

    void showImg(QLabel *label, QString path);
    void showCurrentUI(int index);
    void setButtonImage(QPushButton *button, QString image);

    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);

private slots:
    void on_btnSet_clicked();
    void on_btnMini_clicked();
    void on_btnClose_clicked();
    void onGameMode();
    void onCheckUpdate();
    void onHelp();

private:
    void UiInit();

private:
    Ui::Widget *ui;

    //记录鼠标，窗口位置
    QPoint windowPos;
    QPoint mousePos;
    QPoint dPos;
};
#endif // WIDGET_H
