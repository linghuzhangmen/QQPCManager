﻿#pragma execution_character_set("utf-8")

#include "widget.h"
#include "ui_widget.h"
#include <QProcess>
#include <QDebug>
#include <QAction>
#include <QMenu>
#include <QMessageBox>
#include <QDesktopServices>


Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    //去掉边框
    this->setWindowFlags(Qt::FramelessWindowHint);

    //设置窗口固定大小，无法拖动改变大小
    this->setFixedSize(929, 500);

    //列表控件取消水平竖直滚动条
    ui->listWidget->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->listWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    //列表控件去掉边框
    ui->listWidget->setFrameShape(QFrame::NoFrame);

    UiInit();
}

Widget::~Widget()
{
    delete ui;
}

void Widget::UiInit()
{
    int item_wid = ui->listWidget->width();
    int item_high = (ui->listWidget->height() - 100) / 6;

    QStringList strList;
    strList << tr("首页体检") << tr("病毒查杀") << tr("垃圾清理") << tr("电脑加速") << tr("工具箱") << tr("软件管理");

    for(int i = 0; i < strList.size(); i++)
    {
        QListWidgetItem * pItem = new QListWidgetItem;
        pItem->setSizeHint(QSize(item_wid, item_high));  //设置Item的带下
        pItem->setText(strList.at(i));
        pItem->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter); //文本对齐方式，水平竖直居中
        ui->listWidget->insertItem(i, pItem);
    }

    showImg(ui->label_LOGO, "D:\\Qt_Project\\QQPCManager\\img\\LOGO.png");
    showImg(ui->label_1, "D:\\Qt_Project\\QQPCManager\\img\\1.png");

    //connect(ui->listWidget,SIGNAL(currentRowChanged(int)),ui->stackedWidget,SLOT(setCurrentIndex(int)));

    connect(ui->listWidget,&QListWidget::currentRowChanged, [=](int index)
    {
        //先跳到相应的列表项，再做其它的事，当index == 5需要启动外部进程，会阻塞listwidget item的跳转
        ui->stackedWidget->setCurrentIndex(index);

        if(index == 5)
        {
            QProcess *process = new QProcess(this);

            //【软件管理】无法启动。这里以启动有道词典为例，估计是有权限
            process->start("C:/Users/Administrator/AppData/Local/Youdao/dict/Application/YoudaoDict.exe");
            bool ret = process->waitForStarted();
            if(ret)
            {
                qDebug() << "start ok";
            }
            else
            {
                qDebug() << "start failed";
            }
        }
    });

    connect(ui->stackedWidget, &QStackedWidget::currentChanged, this, &Widget::showCurrentUI);

    //设置右上角3个按钮的图标
    setButtonImage(ui->btnSet, "D:\\Qt_Project\\QQPCManager\\img\\set.png");
    setButtonImage(ui->btnMini, "D:\\Qt_Project\\QQPCManager\\img\\mini.png");
    setButtonImage(ui->btnClose, "D:\\Qt_Project\\QQPCManager\\img\\close.png");
}

void Widget::showImg(QLabel *label, QString path)
{
    QPixmap *pmp = new QPixmap(path);
    pmp->scaled(label->size(), Qt::KeepAspectRatio);
    label->setScaledContents(true);
    label->setPixmap(*pmp);
}

void Widget::showCurrentUI(int index)
{
    QString img = "D:\\Qt_Project\\QQPCManager\\img\\" + QString::number(index + 1) + ".png";
    if(index == 0)
        showImg(ui->label_1, img);
    else if(index == 1)
        showImg(ui->label_2, img);
    else if(index == 2)
        showImg(ui->label_3, img);
    else if(index == 3)
        showImg(ui->label_4, img);
    else if(index == 4)
        showImg(ui->label_5, img);
}

//设置按钮图标
void Widget::setButtonImage(QPushButton *button, QString image)
{
    QPixmap pixmap(image);
    int wid = 20;
    QPixmap fitpixmap = pixmap.scaled(wid, wid).scaled(wid, wid, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
    button->setIcon(QIcon(fitpixmap));
    button->setIconSize(QSize(wid, wid));
    button->setFlat(true);  //按钮透明
    button->setStyleSheet("border: 0px"); //消除边框
}

void Widget::mousePressEvent(QMouseEvent *event)
{
    this->windowPos = this->pos();       // 获得部件当前位置
    this->mousePos = event->globalPos(); // 获得鼠标位置
    this->dPos = mousePos - windowPos;   // 移动后部件所在的位置
}

void Widget::mouseMoveEvent(QMouseEvent *event)
{
    this->move(event->globalPos() - this->dPos);
}

void Widget::on_btnSet_clicked()
{
    QList<QAction*> acList;

    QAction *workMode = new QAction(tr("工作模式"), this);
    QAction *yuleMode = new QAction(tr("娱乐模式"), this);
    QAction *gameMode = new QAction(tr("游戏模式"), this);

    //多级子菜单项
    acList << workMode << yuleMode << gameMode;

    QMenu *modeMenu = new QMenu;
    modeMenu->addActions(acList);
    modeMenu->setTitle(tr("模式设置"));

    QMenu *menuItems = new QMenu;
    menuItems->addMenu(modeMenu);

    QAction *back = new QAction(tr("反馈建议"), this);
    menuItems->addAction(back);

    QAction *setCenter = new QAction(tr("设置中心"), this);
    menuItems->addAction(setCenter);

    QAction *checkUpdate = new QAction(tr("检查更新"), this);
    menuItems->addAction(checkUpdate);

    QAction *forum = new QAction(tr("管家论坛"), this);
    menuItems->addAction(forum);

    QAction *help = new QAction(tr("帮助"), this);
    menuItems->addAction(help);

    //菜单项槽连接
    connect(gameMode, SIGNAL(triggered()), this, SLOT(onGameMode()));
    connect(checkUpdate, SIGNAL(triggered()), this, SLOT(onCheckUpdate()));
    connect(help, SIGNAL(triggered()), this, SLOT(onHelp()));

    ui->btnSet->setMenu(menuItems);
}

void Widget::onGameMode()
{
    QMessageBox::information(this, tr("提示"), "即将进入游戏安全模式");
}

void Widget::onCheckUpdate()
{
    QMessageBox::information(this, tr("提示"), "当前已是最新版本");
}

void Widget::onHelp()
{
    QDesktopServices::openUrl(QUrl(QLatin1String("https://guanjia.qq.com/help/2048.html")));
}

void Widget::on_btnMini_clicked()
{
    showMinimized();
}

void Widget::on_btnClose_clicked()
{
    close();
}

